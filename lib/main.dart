import 'package:flutter/material.dart';
import 'package:httpapi/Models/Users.dart';
import 'package:httpapi/Network_request/network_request.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late List<User> futureUser=<User>[];
  @override
  void initState() {
    super.initState();
    NetWorkRequest.fetchUser().then((dataFromServer) => {
          setState(() {
            futureUser = dataFromServer;
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GET API WITH HTTP',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: const Text('GET API WITH HTTP'),
          ),
          body: Column(
            children: [
              Expanded(
                  child: ListView.builder(
                      padding: const EdgeInsets.all(10),
                      itemCount: futureUser.length,
                      itemBuilder: (context, index) {
                        return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${futureUser[index].id} :'
                                  '${futureUser[index].title}',
                                  style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  '${futureUser[index].body}',
                                  style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue),
                                ),
                              ],
                            ),
                          ),
                        );
                      }))
            ],
          )),
    );
  }
}
