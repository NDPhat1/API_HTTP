import 'dart:convert';

import '../Models/Users.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';


class NetWorkRequest
{
  static const String url='https://jsonplaceholder.typicode.com/posts';
  static List<User> parseUser(String responBody)
  {
    var list=json.decode(responBody) as List<dynamic>;
    List<User> users=list.map((model) => User.fromJson(model)).toList();
    return users;

  }
  static Future<List<User>> fetchUser({int page = 1}) async {
    final response = await http
        .get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return compute(parseUser,response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}